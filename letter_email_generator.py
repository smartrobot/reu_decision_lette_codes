import csv
import os
from shutil import copyfile

csv_file='2019_REU_Decisions.csv'

datafile = open(csv_file, 'r')
datareader = csv.reader(datafile)
data = []
for row in datareader:
    data.append(row) 

#~ try:    
	#~ os.system("rm -r accept/")
#~ except:
	#~ pass
#~ os.mkdir("accept")

#~ try:    
	#~ os.system("rm -r reject/")
#~ except:
	#~ pass
#~ os.mkdir("reject")

try:    
	os.system("rm -r wait/")
except:
	pass
os.mkdir("wait")

 
for i in range(1,len(data)):

	
	if data[i][0]=="a":
		continue
		fr = open("tex_accept/accept.src", "r")
		
		tex=fr.read()
		
		tex=tex.replace("here_xx_here", data[i][3]+" "+data[i][5])#

		fr.close()
		fw = open("tex_accept/accept.tex", "w")
		fw.write(tex)
		fw.close()
		os.chdir("tex_accept/")
		os.system("texmake clean")
		try:
			os.mkdir("../accept/"+data[i][3]+"_"+data[i][5]+"_acceptance_letter_email")
		except:
			pass
		os.system("texmake")
		os.chdir("..")
		email = open("accept/"+data[i][3]+"_"+data[i][5]+"_acceptance_letter_email/email.txt", "w")
		email.write("Dear "+data[i][3]+",\n\n"+"Congratulations! Please see the attached offer letter.\n\nHave a great day!\n\n"+data[i][14]+"\nDecision Notification: REU in Applied Computational Robotics")
		email.close()

		copyfile("tex_accept/accept.pdf", "accept/"+data[i][3]+"_"+data[i][5]+"_acceptance_letter_email/offer.pdf")

	elif data[i][0]=="r":
		continue
		fr = open("tex_reject/reject.src", "r")
		
		tex=fr.read()
		
		tex=tex.replace("here_xx_here", data[i][3]+" "+data[i][5])#

		fr.close()
		fw = open("tex_reject/reject.tex", "w")
		fw.write(tex)
		fw.close()
		os.chdir("tex_reject/")
		os.system("texmake clean")
		try:
			os.mkdir("../reject/"+data[i][3]+"_"+data[i][5]+"_reject_letter_email")
		except:
			pass
		os.system("texmake")
		os.chdir("..")
		email = open("reject/"+data[i][3]+"_"+data[i][5]+"_reject_letter_email/email.txt", "w")
		email.write("Dear "+data[i][3]+",\n\n"+"Please see the attached decision letter.\n\nBest,\n\n"+data[i][14]+"\nDecision Notification: REU in Applied Computational Robotics")
		email.close()

		copyfile("tex_reject/reject.pdf", "reject/"+data[i][3]+"_"+data[i][5]+"_reject_letter_email/decision_letter.pdf")
	elif data[i][0]=="w":
		fr = open("tex_wait/wait.src", "r")
		
		tex=fr.read()
		
		tex=tex.replace("here_xx_here", data[i][3]+" "+data[i][5])#

		fr.close()
		fw = open("tex_wait/wait.tex", "w")
		fw.write(tex)
		fw.close()
		os.chdir("tex_wait/")
		os.system("texmake clean")
		try:
			os.mkdir("../wait/"+data[i][3]+"_"+data[i][5]+"_wait_letter_email")
		except:
			pass
		os.system("texmake")
		os.chdir("..")
		email = open("wait/"+data[i][3]+"_"+data[i][5]+"_wait_letter_email/email.txt", "w")
		email.write("Dear "+data[i][3]+",\n\n"+"Please see the attached decision letter.\n\nBest,\n\n"+data[i][14]+"\nDecision Notification: REU in Applied Computational Robotics")
		email.close()

		copyfile("tex_wait/wait.pdf", "wait/"+data[i][3]+"_"+data[i][5]+"_wait_letter_email/decision_letter.pdf")
	elif data[i][0]=="d":
		print "redundant"
	else:
		raise Exception("something wrong")
